## Introduction

This is a simple pipeline example for an ASP.NET Core application, showing just
how easy it is to get up and running with ASP.NET development using GitLab. This
is inspired from [dotnetcore](https://gitlab.com/gitlab-org/project-templates/dotnetcore) GitLab
template.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [ASP.NET Getting Started tutorial](https://docs.microsoft.com/en-us/aspnet/core/getting-started)

If you're new to ASP.NET you'll want to check out the tutorial, but if you're
already a seasoned developer considering building your own .NET app with GitLab,
this should all look very familiar.

## What's contained in this project

The root of the repository contains the out of the `dotnet new webapp -o aspnetcoreapp` command,
which generates a new ASP.Net core application that just shows a Welcome page.
It's a simple example, but great for demonstrating how easy GitLab CI is to
use with ASP.NET Core. Check out the `Startup.cs`,`Program.cs`  and `aspnetcoreapp.csproj` files to
see how these work.

In addition to the ASP.NET Core content, there is a ready-to-go `.gitignore` file
sourced from the the ASP.NET Core [.gitignore](https://github.com/dotnet/aspnetcore/blob/main/.gitignore). This
will help keep your repository clean of build files and other configuration.

Finally, the `.gitlab-ci.yml` contains the configuration needed for GitLab
to build and publish your code. Let's take a look, section by section.

First, we note that we want to use the official Microsoft .NET SDK image
to build our project.

```
image: mcr.microsoft.com/dotnet/sdk:5.0
```

We're defining two stages here: `build`, and `test`. As your project grows
in complexity you can add more of these.

```
stages:
    - build
    - test
```

Next, we define our build job which simply runs the `dotnet build` command and
identifies the `bin` folder as the output directory. Anything in the `bin` folder
will be automatically handed off to future stages, and is also downloadable through
the web UI.

```
build:
    stage: build
    script:
        - "dotnet build"
    artifacts:
      paths:
        - bin/
```

Similar to the build step, we get our test output simply by running `dotnet test`.

```
test:
    stage: test
    script: 
        - "dotnet test"
```

This should be enough to get you started. There are many, many powerful options 
for your `.gitlab-ci.yml`. You can read about them in our documentation 
[here](https://docs.gitlab.com/ee/ci/yaml/).

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud workspace with .NET Core pre-installed, and your project will automatically be built and start running.
